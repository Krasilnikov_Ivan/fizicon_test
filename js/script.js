(function ($) {
    // В ней хранятся все курсы, полученные от сервера
    var courses;

    $(function () {
        // Получения курсов с сервера
        getCourses();

        $('#genre, #grade, #subj, #bonus-price').on('change', function () {
            var filterCourses = getFilterCourses();
            parseCourses(filterCourses);
        });

        $('#search').on('keyup', function () {
            var filterCourses = getFilterCourses();
            parseCourses(filterCourses);
        })
    });

    function getCourses() {
        $.ajax({
            url: 'http://api.qa.imumk.ru/api/mobilev1/update',
            method: 'POST',
            crossDomain: true,
            dataType: 'jsonp',
            data: $('#filterform').serialize()
        }).done(function (response) {
            courses = response;
            parseCourses(courses);
        }).fail(function (response) {
            console.log('fail');
            console.log(response);
            // Заглушка, на время, пока не работает сервер
            courses = {
                "items": [
                    {
                        "courseId": "105",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Демо-версия",
                        "grade": "8;9;10;11",
                        "genre": "Медиа-коллекция",
                        "subject": "Демо-версия",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 0,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "66",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Демо-версия",
                        "grade": "5;6;7;8;9",
                        "genre": "Рабочая тетрадь",
                        "subject": "Демо-версия",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 0,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "98",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Демо-версия",
                        "grade": "10;11",
                        "genre": "Тренажер ЕГЭ-2016",
                        "subject": "Демо-версия",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 0,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "80",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Алгебра",
                        "grade": "7",
                        "genre": "Рабочая тетрадь",
                        "subject": "Алгебра",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 2000
                    },{
                        "courseId": "81",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Алгебра",
                        "grade": "8",
                        "genre": "Рабочая тетрадь",
                        "subject": "Алгебра",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 1500
                    },{
                        "courseId": "82",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Алгебра",
                        "grade": "9",
                        "genre": "Рабочая тетрадь",
                        "subject": "Алгебра",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 2000
                    },{
                        "courseId": "1618",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Английский язык",
                        "grade": "10;11",
                        "genre": "Тренажер ЕГЭ-2017",
                        "subject": "Английский язык",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 250,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "61",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Биология",
                        "grade": "6",
                        "genre": "Рабочая тетрадь",
                        "subject": "Биология",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "27",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Биология",
                        "grade": "7",
                        "genre": "Рабочая тетрадь",
                        "subject": "Биология",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "50",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Биология",
                        "grade": "8",
                        "genre": "Рабочая тетрадь",
                        "subject": "Биология",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "79",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Биология",
                        "grade": "8;9;10;11",
                        "genre": "Задачник",
                        "subject": "Биология",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 500,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 4000
                    },{
                        "courseId": "51",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Биология",
                        "grade": "9",
                        "genre": "Рабочая тетрадь",
                        "subject": "Биология",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    },{
                        "courseId": "127",
                        "extId": "Physicon_IMUMK_Course_285524",
                        "courseHash": "191153621289246190966820186118178188200176110202548",
                        "title": "Биология",
                        "grade": "10;11",
                        "genre": "Тренажер ЕГЭ-2016",
                        "subject": "Биология",
                        "itunes_id": "ru.physicon.imumk.Physicon_IMUMK_Course_285524",
                        "progress": 0,
                        "description": "",
                        "status": "demo",
                        "price": 200,
                        "shopUrl": null,
                        "google_id": "ru.fizikon.physicon_imumk_course_285524",
                        "winstore_id": null,
                        "isNew": false,
                        "priceBonus": 0
                    }
                ],
                "result": "Ok",
                "errorMessage": null
        };
            parseCourses(courses);
        })
    }

    function parseCourses(data) {
        var coursesBlock = $('#courses');
        var coursesContainer = $('<div />',{'class': 'row'});
        if(data.length !== 0){
            var course;
            for(var i = 0; i < data.items.length; i++){
                var item = data.items[i],
                    grade = item.grade.split(';'),
                    bonusPrice = $('#bonus-price').prop('checked');
                console.log(bonusPrice);
                course = $('<div />',{
                    'class': 'col col-md-2'
                }).append(
                    $('<div />',{
                        'class': 'courses-sci'
                    }).append(
                        $('<div />',{
                            'class': 'sci-figure'
                        }).append(
                            $('<img />', {
                                'src': 'https://www.imumk.ru/covers/' + item.courseId + '.png'
                            })
                        )
                    ).append(
                        $('<div />',{
                            'class': 'sci-info'
                        }).append(
                            $('<p />', {
                                'class': 'sci-title',
                                'text': item.title
                            })
                        ).append(
                            $('<p />', {
                                'class': 'sci-grade',
                                'text': (grade.length == 1) ? item.grade + ' класс' : grade[0] + '-' + grade[grade.length - 1] + ' классы'
                            })
                        ).append(
                            $('<p />', {
                                'class': 'sci-genre',
                                'text': item.genre
                            })
                        ).append(
                            $('<p />', {
                                'class': 'sci-meta'
                            }).append(
                                $('<a />',{
                                    'href': (item.price == 0) ? '/player/' + item.courseId : '/course/' + item.courseId,
                                    'text': (item.price == 0) ? 'Перейти к обучению' : 'Подробнее'
                                })
                            )
                        ).append(
                            $('<p />', {
                                'class': 'sci-controls'
                            }).append(
                                $('<a />',{
                                    'href': (item.price == 0) ? '/course/' + item.courseId : '#',
                                    'text': (item.price == 0) ? 'Подробнее' : (bonusPrice && item.priceBonus !== 0) ? 'Купить за ' + item.priceBonus + ' бон.' : 'Купить за ' + item.price + ' руб.',
                                    'class': 'pure-button btn btn-sm w80'
                                })
                            )
                        )
                    )
                );
                coursesContainer.append(course);
            }
        }
        coursesBlock.empty();
        coursesBlock.append(coursesContainer);
    }

    function getFilterCourses() {
        var filterCourses = Object.create(courses),
            value;
        if($('#genre').val() !== 'All'){
            value = $('#genre').val();
            filterCourses.items = filterCourses.items.filter(function (course, index){
                return (course.genre === value);
            });
        }
        if($('#grade').val() !== 'All'){
            value = $('#grade').val();
            filterCourses.items = filterCourses.items.filter(function (course, index){
                return (course.grade.indexOf(value) !== -1);
            });
        }
        if($('#subj').val() !== 'All'){
            value = $('#subj').val();
            filterCourses.items = filterCourses.items.filter(function (course, index){
                return (course.subject === value);
            });
        }
        if($('#search').val() !== ''){
            value = $('#search').val();
            filterCourses.items = filterCourses.items.filter(function (course, index){
                return (course.title.toLowerCase().indexOf(value.toLowerCase()) !== -1);
            });
        }
        return filterCourses;
    }
})(jQuery);